from random import randint

name = input("Hi! What is your name? ")

month_number = randint(1, 12)
year_number = randint(1924, 2004)

for i in range(1, 6):
    print("Guess", i , ": ", name, "were you born in",
        month_number, "/", year_number, "?")

    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        exit()
    elif i == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")
